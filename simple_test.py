# from jupyter notebook
# conda env: segmentation

import numpy as np
import matplotlib.pyplot as plt
from patchify import patchify

import torch
import torch.nn as nn
from torch.utils.data import DataLoader,Subset
from torch.autograd import Variable
from torchvision import models
from torch.utils.data import Dataset

from utils.data_utils import load_tomogram
from model import UNet3D_module
from utils.dataloader import Sim_CET
from utils.train import *

def main():
    print("-->>>---",flush=True)
    # print(nvidia-smi,flush=True)
    # setting device on GPU if available, else CPU
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print('Using device:', device, flush=True)

    mask_path = "/home/haicu/ye.liu/data/cryoET/onlyactin/tomo_lbls_0.mrc"
    img_path = "/home/haicu/ye.liu/data/cryoET/onlyactin/tomo_rec_0_snr1.7.mrc"
    patch_size = (64,64,64)
    num_patches = 675
    # num_patches = 6*30*30

    # Setup parameters
    # n_fold = 5
    # pad_left = 27
    # pad_right = 27
    # fine_size = 202
    batch_size = 4
    epoch = 50 #300 
    snapshot = 6 
    max_lr = 0.01 #0.0001(5188.job), 0.012 
    min_lr = 0.001 
    momentum = 0.9 
    weight_decay = 1e-4 

    # patch_size, input channel, output channel(greyscale -> #num of channel =1)
    model = UNet3D_module(patch_size,1,1).to(device)

    # dataset & dataloader 
    dataset = Sim_CET(img_path, mask_path, patch_size, num_patches)
    # train_set, test_set = dataset.split()
    train_set = torch.utils.data.Subset(dataset,range(np.int32(len(dataset)*0.5)))
    test_set = torch.utils.data.Subset(dataset,range(np.int32(len(dataset)*0.5),len(dataset)))
    # train_set, test_set = torch.utils.data.random_split(dataset, [int(np.floor(len(dataset)*0.8)), int(np.ceil(len(dataset)*0.2))])

    train_loader = DataLoader(train_set, batch_size = batch_size)
    test_loader = DataLoader(test_set, batch_size=batch_size)

    # Setup optimizer
    scheduler_step = epoch // snapshot
    optimizer = torch.optim.SGD(model.parameters(), lr=max_lr, momentum=momentum, weight_decay=weight_decay)
    lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, scheduler_step, min_lr)

    # num_snapshot = 0
    # best_acc = 0

    for epoch_ in range(epoch):
        train_loss = train(train_loader, model,train_set,optimizer, device)
        val_loss = test(test_loader,test_set, model, device, epoch_)
        lr_scheduler.step()
        print(lr_scheduler,"--",flush=True)
        
    #     if accuracy > best_acc:
    #         best_acc = accuracy
    #         best_param = model.state_dict()

        if (epoch_ + 1) % scheduler_step == 0:
            print("epoch--")
    # #         torch.save(best_param, save_weight + weight_name + str(idx) + str(num_snapshot) + '.pth')
    #         optimizer = torch.optim.SGD(model.parameters(), lr=max_lr, momentum=momentum, weight_decay=weight_decay)
    #         lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, scheduler_step, min_lr)
    #         num_snapshot += 1
    #         best_acc = 0
        
        best_param = model.state_dict()
        print('epoch: {} train_loss: {:.3f} val_loss: {:.3f}'.format(epoch_ + 1, train_loss, val_loss),flush=True)

        if (epoch_ % 10) == 0:
            save_weight = '/home/haicu/ye.liu/aproject/weights/'
            weight_name = 'model_' + str(epoch_) + '_res18' 
            torch.save(best_param, save_weight + weight_name + '.pth')

if __name__ == '__main__':
  main()
