import torch
import numpy as np
from patchify import patchify
from torch.utils.data import Dataset

from utils.data_utils import load_tomogram

class Sim_CET_Dataset(Dataset):
    def __init__(self, img_path, mask_path, patch_size, ):
        self.img_path = img_path
        self.mask_path = mask_path 
        self.patch_size = patch_size
        # self.transformer = transforms
        # self.num_patches = num_patches
        
        img = load_tomogram(self.img_path[idx])
        # image_patches_tensor = self.transformer(patches[patch_idx])
        mask = load_tomogram(self.mask_path[idx])

        img_patches = patchify(img, (self.patch_size, self.patch_size, 3), step=self.patch_size)
        mask_patches =  patchify(mask, (self.patch_size, self.patch_size, 3), step=self.patch_size)
    
    def __len__(self):
        # return the number of total sampels contained in the dataset
        return len(self.img_path)
        
    
    def __getitem__(self, idx: int):
        img_idx = idx // self.num_patches
        patch_idx = idx % self.num_patches

        image = self.images[img_idx]
        img = load_tomogram(self.img_path[idx])
        # image_patches_tensor = self.transformer(patches[patch_idx])
        mask = load_tomogram(self.mask_path[idx])

        img_patches = patchify(image, (self.patch_size, self.patch_size, 3), step=self.patch_size)
        mask_patches =  patchify(mask, (self.patch_size, self.patch_size, 3), step=self.patch_size)
        
        return (img, mask)

class Sim_CET(Dataset):
    # for simulated dataset, for a single tomogram
    def __init__(self,img_path, mask_path, patch_size, num_patches):
        self.img_path = img_path
        self.mask_path = mask_path
        self.patch_size = patch_size
        self.num_patches = num_patches
        self.img = load_tomogram(self.img_path)
        self.mask = load_tomogram(self.mask_path)

    def __len__(self):
        return self.num_patches
    
    def __getitem__(self, idx:int):
        """
        patchify the tomogram and mask, and iterate through all the patches

        Output: 
        input_tomo: numpy array, [num of patches, patch_size(3-tuple)]
        input_mask: numpy array, [num of patches, patch_size]
        """
        img_patches = patchify(self.img, self.patch_size, step = self.patch_size[0])
        mask_patches = patchify(self.mask, self.patch_size, step = self.patch_size[0])
        print(mask_patches.shape, flush=True)
        
        self.input_mask = np.reshape(mask_patches, (-1, mask_patches.shape[3], mask_patches.shape[4], mask_patches.shape[5]))
        self.input_tomo = np.reshape(img_patches, (-1, img_patches.shape[3], img_patches.shape[4], img_patches.shape[5]))
        print("---input_tomo--",self.input_tomo.shape,flush=True) 
        
        return (self.input_tomo[idx-1][np.newaxis, :,:, :], self.input_mask[idx-1][np.newaxis, :,:, :])
    
    # def split(self, ratio=0.5):
    #     """
    #     cut the same tomogram into 2 with given ratio. One for training, and the other one for testing.

    #     Output:
    #     train_tomo: torch.tensor, [num of patches, patch_size(3-tuple)]
    #     test_tomo: torch.tensor, [num of patches, patch_size(3-tuple)]
    #     train_mask: torch.tensor, [num of patches, patch_size(3-tuple)]
    #     test_mask: torch.tensor, [num of patches, patch_size(3-tuple)]

    #     """
    #     # size = np.int(self.input_mask.shape[0] * ratio)
    #     tomo = np.array_split(self.input_tomo, 2, axis=0)
    #     train_tomo, test_tomo = tomo[0], tomo[1]

    #     mask = np.array_split(self.input_mask, 2, axis=0)
    #     train_mask, test_mask = mask[0], mask[1]

    #     return ((train_tomo,train_mask),(test_tomo,test_mask))



