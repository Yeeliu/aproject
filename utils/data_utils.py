import os
import numpy as np
import csv
import mrcfile

def load_tomogram(filename):
    """
    Loads .mrc tomogram and stores as np.array
    input: 
        filename: string
    output: 
        data : np.array 
    """
    print("Loading tomogram:", filename)

    with mrcfile.open(filename, permissive=True) as mrc:
        data = np.array(mrc.data)
    return data 

def write_mrc(array, filename):
    """
    store np.array as .mrc tomogram  
    input: 
        array: numpy array 
        filename : string 
    """
    with mrcfile.new(filename, overwrite=True) as mrc:
        mrc.set_data(array)