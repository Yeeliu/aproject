import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from utils.losses import DiceLoss
from utils.data_utils import write_mrc
import numpy as np

# helper function
def train(train_loader, model, dataset, optimizer, device):
    running_loss = 0.0
    data_size = len(dataset)
    # print("---train-size--",data_size)
    model.train()
    i=0
    for inputs, masks in train_loader:
        inputs, masks = inputs.to(device), masks.to(device)
        optimizer.zero_grad()
        i+=1

#     with torch.set_grad_enabled(True):
        logit = model(inputs)
        # loss = nn.BCEWithLogitsLoss()(logit.squeeze(1), masks.squeeze(1))
        loss = DiceLoss()(logit.squeeze(1), masks.squeeze(1))
        loss.backward()
        optimizer.step()
        # if (i % 50 ==0):
        #     print("---i----",i,flush=True)
        # if i ==5:  break

    running_loss += loss.item() * inputs.size(0)
    epoch_loss = running_loss / data_size
    # print("---",running_loss,"----",flush=True)
    
    return epoch_loss

def test(test_loader, dataset, model, device, epoch_):
    running_loss = 0.0
    data_size = len(dataset)
    # print("---test-size--",data_size)
    predicts = []
    # truths = []

    model.eval()
    j = 0 

    for inputs, masks in test_loader:
       j+=1
       inputs, masks = inputs.to(device), masks.to(device)
       
       # with torch.set_grad_enabled(False):
       outputs = model(inputs)
       
       if j % 20 == 0:
         file_name_1 = '/home/haicu/ye.liu/aproject/outputs/inputs_' + str(epoch_) + str(j) +'.mrc'
         file_name_2 = '/home/haicu/ye.liu/aproject/outputs/outputs_' + str(epoch_) + str(j)+ '.mrc'
         print("input shape", inputs.cpu().detach().numpy().shape)
         print("output shape", outputs.cpu().detach().numpy().shape)
         write_mrc(inputs.cpu().detach().numpy()[1][0], file_name_1)
         write_mrc(outputs.cpu().detach().numpy()[1][0], file_name_2)
       
    #    outputs = outputs[:, :, pad_left:pad_left + fine_size, pad_left:pad_left + fine_size].contiguous()
    #    loss = nn.BCEWithLogitsLoss()(outputs.squeeze(1), masks.squeeze(1))
       loss = DiceLoss()(outputs.squeeze(1), masks.squeeze(1))
       predicts.append(torch.sigmoid(outputs).detach().cpu().numpy()) 
    #    truths.append(masks.detach().cpu().numpy())
       running_loss += loss.item() * inputs.size(0)
    #    print("-- running_loss_val --- ", running_loss)
        
    # predicts = np.concatenate(predicts).squeeze()
    # truths = np.concatenate(truths).squeeze()
    # precision, _, _ = do_kaggle_metric(predicts, truths, 0.5)
    # precision = precision.mean()
    epoch_loss = running_loss / data_size
    
    return epoch_loss